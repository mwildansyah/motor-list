package id.co.atozpay.atozprinter;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;


public class BluetoothPrinter {

    public static final int REQUEST_ENABLE_BT = 0;
    private static final UUID SPP_UUID = UUID
            .fromString("00001101-0000-1000-8000-00805F9B34FB");
    private static BluetoothSocket btsocket;
    private static BluetoothDevice btdevice;

    private static OutputStream outputStream;

    private Activity mActivity;
    private Context mContext;

    static private BluetoothAdapter mbtAdapter = null;

    public static final int BLUETOOTH_ENABLED = 0;// bt aktif not pair
    public static final int BLUETOOTH_DISABLED = 1;//bt tidak aktif
    public static final int BLUETOOTH_PAIRED = 3; // bt aktif paired

    private static int statusPrinter = 1;

    private OnChangeStatus onChangeStatusSocket;

    public BluetoothPrinter(Activity mActivity, boolean isParing) {
        this.mActivity = mActivity;
        this.mContext = mActivity;
        mbtAdapter = BluetoothAdapter.getDefaultAdapter();
        if(isParing) {
            if (mbtAdapter.isEnabled()) {
                if (getStatusPrinter() != BLUETOOTH_PAIRED && getStatusPrinter() != BLUETOOTH_ENABLED) {
                    setStatusPrinter(BLUETOOTH_ENABLED);
                }
                if (getStatusPrinter() != BLUETOOTH_PAIRED) {
                    Thread thread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            checkPaired();
                        }
                    });
                    thread.start();
                }
            } else {
                setStatusPrinter(BLUETOOTH_DISABLED);
            }
        }
    }


    public BluetoothPrinter(Activity mActivity) {
        this(mActivity, true);
    }

    public  BluetoothSocket getBtsocket() {
        return btsocket;
    }

    public  void setBtsocket(BluetoothSocket btsocket) {
        BluetoothPrinter.btsocket = btsocket;
    }

    public  BluetoothDevice getBtdevice() {
        return btdevice;
    }

    public  void setBtdevice(BluetoothDevice btdevice) {
        BluetoothPrinter.btdevice = btdevice;
    }

    public OnChangeStatus getOnChangeStatusSocket() {
        return onChangeStatusSocket;
    }

    public void setOnChangeStatusSocket(OnChangeStatus onChangeStatusSocket) {
        this.onChangeStatusSocket = onChangeStatusSocket;
    }


    public interface OnChangeStatus {
        public abstract void onSocketError();
        public abstract void onBluetoothPaired();
        public abstract void onBluetoothDisconect();
        public abstract void onBluetootEnabled();
    }

    public void deviceConectting(final BluetoothDevice bluetoothDevice) throws IOException {

        Thread connectThread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    setBtdevice(bluetoothDevice);
                    setBtsocket(bluetoothDevice.createRfcommSocketToServiceRecord(SPP_UUID));
                    getBtsocket().connect();
                    setStatusPrinter(BLUETOOTH_PAIRED);
                } catch (IOException ex) {
                    ex.printStackTrace();
                    mActivity.runOnUiThread(socketErrorRunnable);
                    try {
                        setStatusPrinter(BLUETOOTH_ENABLED);
                        getBtsocket().close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    setBtsocket(null);
                } finally {
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mActivity.finish();
                        }
                    });
                }
            }
        });

        connectThread.start();
    }


    private Runnable socketErrorRunnable = new Runnable() {

        @Override
        public void run() {
            Toast.makeText(mActivity.getApplicationContext(),
                    "Cannot establish connection", Toast.LENGTH_SHORT).show();
            if (onChangeStatusSocket != null) {
                onChangeStatusSocket.onSocketError();
//                mBluetoothAdapter.startDiscovery();
            }

        }
    };


    public void checkPaired() {
        try {
            Set<BluetoothDevice> pairedDevice = mbtAdapter.getBondedDevices();
            if (pairedDevice.size() > 0) {
                for (BluetoothDevice pairedDev : pairedDevice) {
                    if (pairedDev.getBondState() == 12) {
                        try {
//                            BluetoothSocket tempBtsocket = DeviceList.getSocket();
                            BluetoothSocket tempBtsocket = pairedDev.createRfcommSocketToServiceRecord(SPP_UUID);
                            if (tempBtsocket.isConnected()) {
                                setBtsocket(tempBtsocket);
                                setBtdevice(pairedDev);
                                setStatusPrinter(BLUETOOTH_PAIRED);
                                break;
                            }else{
                                tempBtsocket.connect();
                                if (tempBtsocket.isConnected()) {
                                    setBtsocket(tempBtsocket);
                                    setBtdevice(pairedDev);
                                    setStatusPrinter(BLUETOOTH_PAIRED);
                                    break;
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    public void turnONBT() {
        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        mActivity.startActivityForResult(enableBtIntent, 0);
    }


    public void toastStatus(int statusBluetooth) {
        switch (statusBluetooth) {
            case BLUETOOTH_ENABLED:
                Toast.makeText(mActivity, "Bluetooth is ON!!", Toast.LENGTH_SHORT).show();
                break;
            case BLUETOOTH_DISABLED:
                Toast.makeText(mActivity, "Bluetooth is OFF!!", Toast.LENGTH_SHORT).show();
                break;
            case BLUETOOTH_PAIRED:
                Toast.makeText(mContext, "Paired", Toast.LENGTH_SHORT).show();
                break;
               }
    }


    public void searchBluetooth() {
//        if (btsocket == null) {
            Intent BTIntent = new Intent(mActivity, DeviceList.class);
            mActivity.startActivityForResult(BTIntent, DeviceList.REQUEST_CONNECT_BT);
//        } else {
//            OutputStream opstream = null;
//            try {
//                opstream = btsocket.getOutputStream();
//            } catch (IOException ex) {
//                ex.printStackTrace();
//            }
//            outputStream = opstream;
//        }
    }


    /**
     * Gets resized bitmap.
     *
     * @param bmp the bmp
     * @return the resized bitmap
     */
    public Bitmap getResizedBitmap(Bitmap bmp) {
        double ratio = ((double) bmp.getWidth()) / ((double) bmp.getHeight());
        double newWidth = (double) bmp.getWidth();
        double newHeight = (double) bmp.getHeight();
        if (newWidth > 384.0d) {
            newWidth = 384.0d;
            newHeight = 384.0d / ratio;
        }
        return Bitmap.createScaledBitmap(bmp, (int) newWidth, (int) newHeight, true);
    }

    // Printing Photo to Bluetooth Printer //f
    public void printPhoto(int img, int bg) {
        Bitmap bmp = BitmapFactory.decodeResource(mContext.getResources(), img);
        bmp = getResizedBitmap(bmp);
        try {
            if (bmp != null) {
                byte[] command = Utils.decodeBitmap(bmp, bg);
                try {
                    OutputStream opstream = null;
                    try {
//                                btsocket = DeviceList.getSocket();
                        opstream = getBtsocket().getOutputStream();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    outputStream = opstream;
                    try {
                        outputStream.write(PrinterCommands.ESC_ALIGN_CENTER);
                        printText(command);
                        printNewLine();
                        outputStream.flush();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            } else {
                Log.e("Print Photo error", "the file isn't exists");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("PrintTools", "the file isn't exists");
        }

    }

    //print new line
    public void printNewLine() {
        try {
            OutputStream opstream = null;
            try {
//                btsocket = DeviceList.getSocket();
                opstream = getBtsocket().getOutputStream();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            outputStream = opstream;
            try {
                outputStream.write(PrinterCommands.FEED_LINE);
                outputStream.flush();
            } catch (IOException ex) {
                ex.printStackTrace();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    //print text
    public void printText(String msg) {
        try {
            OutputStream opstream = null;
            try {
//                btsocket = DeviceList.getSocket();
                if(getBtsocket()==null){
                    Log.d("Print", "null");
                    setStatusPrinter(BLUETOOTH_ENABLED);
                    toastStatus(getStatusPrinter());
                    return;
                }
                opstream = getBtsocket().getOutputStream();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            outputStream = opstream;
            try {
                outputStream.write(msg.getBytes());
                printNewLine();
                outputStream.flush();
            } catch (IOException ex) {
                ex.printStackTrace();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    //    print byte[]
    public void printText(byte[] msg) {
        try {
            OutputStream opstream = null;
            try {
//                btsocket = DeviceList.getSocket();
                if(getBtsocket()==null){
                    Log.d("Print", "null");
                    setStatusPrinter(BLUETOOTH_ENABLED);
                    toastStatus(getStatusPrinter());
                    return;
                }
                opstream = getBtsocket().getOutputStream();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            outputStream = opstream;
            try {
                outputStream.write(msg);
                outputStream.flush();
            } catch (IOException ex) {
                ex.printStackTrace();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    //print custom
    public void printCustom(String msg, int size, int align) {
        try {
            OutputStream opstream = null;
            try {
//                setBtsocket(DeviceList.getSocket());
                if(getBtsocket()==null){
                    Log.d("Print", "null");
                    setStatusPrinter(BLUETOOTH_ENABLED);
                    toastStatus(getStatusPrinter());
                    return;
                }
                opstream = getBtsocket().getOutputStream();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            outputStream = opstream;
            try {
                //Print config "mode"
//                byte[] S = new byte[]{0x1B, 0x21, 0x03};  // 0- normal size text
//                byte[] S = new byte[]{0x1B, 0x21, 0x06};  // 0- normal size text
//                byte[] M = new byte[]{0x1B, 0x21, 0x08};  // 1- only bold text
//                byte[] L = new byte[]{0x1B, 0x21, 0x10}; // 2- bold with medium text
//                byte[] XL = new byte[]{0x1B, 0x21, 0x20}; // 3- bold with large text


                byte[] S = new byte[]{0x1B, 0x21, 0x04};  // 0- normal size text
                byte[] M = new byte[]{0x1B, 0x21, 0x08};  // 1- only bold text
                byte[] L = new byte[]{0x1B, 0x21, 0x10}; // 2- bold with medium text
                byte[] XL = new byte[]{0x1B, 0x21, 0x20}; // 3- bold with large text


//                byte[] S = new byte[]{0x1B, 0x21, 0x04};  // 0- normal size text
//                byte[] M = new byte[]{0x1B, 0x21, 0x04};  // 1- only bold text
//                byte[] L = new byte[]{0x1B, 0x21, 0x04}; // 2- bold with medium text
//                byte[] XL = new byte[]{0x1B, 0x21, 0x04}; // 3- bold with large text

                try {
                    switch (size) {
                        case 0:
                            outputStream.write(S);
                            break;
                        case 1:
                            outputStream.write(M);
                            break;
                        case 2:
                            outputStream.write(L);
                            break;
                        case 3:
                            outputStream.write(XL);
                            break;
                    }

                    switch (align) {
                        case 0:
                            //left align
                            outputStream.write(PrinterCommands.ESC_ALIGN_LEFT);
                            break;
                        case 1:
                            //center align
                            outputStream.write(PrinterCommands.ESC_ALIGN_CENTER);
                            break;
                        case 2:
                            //right align
                            outputStream.write(PrinterCommands.ESC_ALIGN_RIGHT);
                            break;
                    }
                    outputStream.write(msg.getBytes());
                    outputStream.write(PrinterCommands.LF);
                    //outputStream.write(cc);
                    //printNewLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    // Printing Text to Bluetooth Printer //
    public void printData() {
        try {
            OutputStream opstream = null;
            try {
//                btsocket = DeviceList.getSocket();
                opstream = getBtsocket().getOutputStream();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            outputStream = opstream;
            try {

                outputStream.write(PrinterCommands.ESC_ALIGN_CENTER);
                outputStream.flush();
            } catch (IOException ex) {
                ex.printStackTrace();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public int getStatusPrinter() {
        return statusPrinter;
    }

    public void setStatusPrinter(int statusPrinter) {
        this.statusPrinter = statusPrinter;
        if(getOnChangeStatusSocket()!= null) {
            if (statusPrinter == BLUETOOTH_ENABLED) {
                getOnChangeStatusSocket().onBluetootEnabled();
            }else if(statusPrinter == BLUETOOTH_PAIRED){
                getOnChangeStatusSocket().onBluetoothPaired();
            } else if(statusPrinter == BLUETOOTH_DISABLED) {
                getOnChangeStatusSocket().onBluetoothDisconect();

            }
        }

//        toastStatus(statusPrinter);
        Log.d("Printer", statusPrinter + "");
    }

    public void bluetoothDisconect(){
        try {
            btsocket.close();
            btdevice = null;
            setStatusPrinter(BLUETOOTH_ENABLED);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent dtta) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        Resources mActivityResources = mActivity.getResources();
//        switch (requestCode){
//            case REQUEST_ENABLE_BT:
//                if (resultCode == RESULT_OK) {
//                    tvStatus.setText("Bluetooth Status : ON");
//                    Button btnTurnOn = (Button)findViewById(R.id.btnTurnOn);
//                    btnTurnOn.setEnabled(false);
//                }
//        }
//        try {
//            btsocket = DeviceList.getSocket();
//            btdevice = DeviceList.getDevices();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        if (btsocket != null){
//            Button btnSearch = (Button)findViewById(R.id.btnSearch);
//            btnSearch.setEnabled(false);
//        }
//
//        if (btdevice != null){
//            tvPaired.setText("Bluetooth Paired : " + btdevice.getName() +"  "+ btdevice.getAddress());
//        }
//    }
}