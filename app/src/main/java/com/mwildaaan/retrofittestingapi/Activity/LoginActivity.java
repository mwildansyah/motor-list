package com.mwildaaan.retrofittestingapi.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.mwildaaan.retrofittestingapi.Network.ApiClient;
import com.mwildaaan.retrofittestingapi.Network.ApiInterface;
import com.mwildaaan.retrofittestingapi.Network.NetworkDetector;
import com.mwildaaan.retrofittestingapi.R;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    EditText etEmail;
    EditText etPassword;
    Button btnLogin;
    ProgressDialog loading;
    RelativeLayout loginActivity;

    NetworkDetector nd;

    Context mContext;
    ApiInterface mApiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();

        etEmail = (EditText)findViewById(R.id.etEmail);
        etPassword = (EditText)findViewById(R.id.etPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        loginActivity = findViewById(R.id.login_activity_id);

        nd = new NetworkDetector(this);

        mContext = this;

        mApiInterface = ApiClient.getService();

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (nd.isConnected()){
                    Toast.makeText(mContext, "Connected", Toast.LENGTH_SHORT).show();
                    loading = ProgressDialog.show(mContext, null, "Please Wait", true, false);
                    requestLogin();
                }else{
                    Toast.makeText(mContext, "Not Connected", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void requestLogin() {

        String email = etEmail.getText().toString().trim();
        String password = etPassword.getText().toString().trim();

        if (email.isEmpty()){
            etEmail.setError("Email is required");
            etEmail.requestFocus();
            return;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            etEmail.setError("Insert valid email");
            etEmail.requestFocus();
            return;
        }

        if (password.isEmpty()){
            etPassword.setError("Password is required");
            etPassword.requestFocus();
        }

        mApiInterface.loginRequest(email, password)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        loading.dismiss();
                        if (response.isSuccessful()){
                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.e("debug", "onFailure: ERROR > " + t.toString());
                        loading.dismiss();
                    }
                });
    }
}
