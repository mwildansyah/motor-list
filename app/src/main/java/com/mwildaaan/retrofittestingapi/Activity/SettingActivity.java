package com.mwildaaan.retrofittestingapi.Activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.mwildaaan.retrofittestingapi.Fragment.DialogFragment;
import com.mwildaaan.retrofittestingapi.R;


public class SettingActivity extends Activity {
    String[] fontSize = {"Small",
                        "Medium",
                        "Large",
                        "Xtra Large"};

    private Button btnFontSize;
    private ArrayAdapter<String> adapter;
    private static int size;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        btnFontSize = (Button)findViewById(R.id.btnFontSize);
        btnFontSize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fontSize();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent setting = new Intent(SettingActivity.this, MainActivity.class);
        startActivity(setting);
        finish();
    }

    public static int getSize() {
        return size;
    }

    private void fontSize() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        View row = getLayoutInflater().inflate(R.layout.row_font_size, null);

        ListView lv = (ListView)row.findViewById(R.id.list_font_size);

        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, fontSize);
        lv.setAdapter(adapter);


        adapter.notifyDataSetChanged();

        alertDialog.setView(row);
        final AlertDialog dialog = alertDialog.create();
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                size= (int)adapterView.getItemIdAtPosition(i);
                dialog.dismiss();
                onBackPressed();
            }
        });
        dialog.show();

    }
}
