package com.mwildaaan.retrofittestingapi.Activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.app.AlertDialog;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.mwildaaan.retrofittestingapi.Adapter.ViewPagerAdapter;
import com.mwildaaan.retrofittestingapi.Fragment.FragmentBebekFC;
import com.mwildaaan.retrofittestingapi.Fragment.FragmentMaticFC;
import com.mwildaaan.retrofittestingapi.Fragment.FragmentSportFC;
import com.mwildaaan.retrofittestingapi.R;

import id.co.atozpay.atozprinter.BluetoothPrinter;
import id.co.atozpay.atozprinter.DeviceList;

public class MainActivity extends AppCompatActivity{

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPagerAdapter adapter;
//    private BluetoothSocket btsocket;
//    private BluetoothDevice btdevice;
    private Button btnTurnOnBT, btnSearchBT;
    private Context mContext;
    private AlertDialog.Builder alertDialog;
    private int size;

    private BluetoothPrinter bluetoothPrinter;

    private static final String[] DATASET = new String[]{
            "bebek", "sport", "matic"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       setBluetoothPrinter(new BluetoothPrinter(this));
       getBluetoothPrinter().setOnChangeStatusSocket(new BluetoothPrinter.OnChangeStatus() {
                                                         @Override
                                                         public void onSocketError() {

                                                         }

                                                         @Override
                                                         public void onBluetoothPaired() {
                                                             checkStatusBluetooth();
                                                         }

                                                         @Override
                                                         public void onBluetoothDisconect() {
                                                             checkStatusBluetooth();
                                                         }

                                                         @Override
                                                         public void onBluetootEnabled() {
                                                            checkStatusBluetooth();
                                                         }
                                                     });


        // Button Setting
        Button btnSetting = (Button)findViewById(R.id.btnSetting);
        tabLayout = (TabLayout)findViewById(R.id.tabLayout_id);
        viewPager = (ViewPager) findViewById(R.id.viewPager_id);
        // Button Check Paired
        Button btnCheckPaired = (Button)findViewById(R.id.btnCheckPaired);

        // Button Turn On Bluetooth
        btnTurnOnBT = (Button)findViewById(R.id.btnTurnOnBT);
        //Button Search Bluetooth
        Button btnSearchBT= (Button)findViewById(R.id.btnSearchBT);

        btnSearchBT.setVisibility(View.GONE);
        btnCheckPaired.setVisibility(View.GONE);


        btnSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent setting = new Intent(MainActivity.this, SettingActivity.class);
                startActivity(setting);
                finish();
            }
        });

        btnCheckPaired.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getBluetoothPrinter().checkPaired();
            }
        });

//        if (btAdapter.isEnabled()){
//            btnTurnOnBT.setText("Bluetooth is ON!!");
//            btnTurnOnBT.setEnabled(false);
//            bluetoothPrinter.setBluetoothStatus(0);
//        }else {
//            btnTurnOnBT.setEnabled(true);
//            bluetoothPrinter.setBluetoothStatus(1);
//        }

        btnTurnOnBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                     getBluetoothPrinter().turnONBT();
            }
        });

        btnSearchBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getBluetoothPrinter().searchBluetooth();
            }
        });

        adapter = new ViewPagerAdapter(getSupportFragmentManager());

        adapter.AddFragment(new FragmentSportFC(),"Sport");
        adapter.AddFragment(new FragmentMaticFC(),"Matic");
        adapter.AddFragment(new FragmentBebekFC(),"Bebek");
        viewPager.setOffscreenPageLimit(2);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setElevation(0);

    }

    @Override
    protected void onResume() {
        super.onResume();
        checkStatusBluetooth();
    }

    private void checkStatusBluetooth(){
        if(bluetoothPrinter.getStatusPrinter()==BluetoothPrinter.BLUETOOTH_DISABLED){
            btnTurnOnBT.setText("Aktif Bluettoth");
            btnTurnOnBT.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getBluetoothPrinter().turnONBT();
                }
            });
        }else if(bluetoothPrinter.getStatusPrinter()==BluetoothPrinter.BLUETOOTH_ENABLED){
            btnTurnOnBT.setText("Search & Paired");
            btnTurnOnBT.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getBluetoothPrinter().searchBluetooth();
                }
            });
        }else if(bluetoothPrinter.getStatusPrinter()==BluetoothPrinter.BLUETOOTH_PAIRED){
            btnTurnOnBT.setText("Disconected");
            btnTurnOnBT.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getBluetoothPrinter().bluetoothDisconect();
                }
            });
//        }else if(bluetoothPrinter.getStatusPrinter()==BluetoothPrinter.BLUETOOTH_DISABLED){
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 0:
                if (resultCode == RESULT_OK) {
                    Button btnTurnOnBT = (Button)findViewById(R.id.btnTurnOnBT);
                    btnTurnOnBT.setText("Bluetooth is ON!!");
                }
        }

        try {
//            bluetoothPrinter.setBtdevice(DeviceList.getDevices());
//            bluetoothPrinter.setBtsocket(DeviceList.getSocket());
//            btsocket = DeviceList.getSocket();
//            btdevice = DeviceList.getDevices();
        } catch (Exception e) {
            e.printStackTrace();
        }

//        if (btsocket != null){
//            Button btnSearchBT = (Button)findViewById(R.id.btnSearchBT);
//            btnSearchBT.setEnabled(false);
//        }
        if(bluetoothPrinter.getBtdevice() != null){
            bluetoothPrinter.setStatusPrinter(BluetoothPrinter.BLUETOOTH_PAIRED);
            TextView tvPairedBT = (TextView)findViewById(R.id.tvPairedBT);
            tvPairedBT.setText("Paired : "+bluetoothPrinter.getBtdevice().getName()+" - "+bluetoothPrinter.getBtdevice().getAddress());
        }
    }

    public BluetoothPrinter getBluetoothPrinter() {
        return bluetoothPrinter;
    }

    public void setBluetoothPrinter(BluetoothPrinter bluetoothPrinter) {
        this.bluetoothPrinter = bluetoothPrinter;
    }
}
