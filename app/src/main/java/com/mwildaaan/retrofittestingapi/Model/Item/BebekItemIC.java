package com.mwildaaan.retrofittestingapi.Model.Item;

import com.google.gson.annotations.SerializedName;


import com.google.gson.annotations.Expose;

public class BebekItemIC extends MotorItem {

    @SerializedName("gear")
    @Expose
    protected Integer gear;

    @SerializedName("kopling")
    @Expose
    private Boolean kopling;

    public BebekItemIC(){

    }

    public BebekItemIC(Integer id, String motorType, String urlImage, Integer price, String mesinCapacity, Integer gear, Boolean kopling) {
        this.id = id;
        this.motorType = motorType;
        this.urlImage = urlImage;
        this.price = price;
        this.mesinCapacity = mesinCapacity;
        this.gear = gear;
        this.kopling = kopling;
    }

    public Integer getGear() {
        return gear;
    }

    public void setGear(Integer gear) {
        this.gear = gear;
    }

    public Boolean getKopling() {
        return kopling;
    }

    public void setKopling(Boolean kopling) {
        this.kopling = kopling;
    }



}
