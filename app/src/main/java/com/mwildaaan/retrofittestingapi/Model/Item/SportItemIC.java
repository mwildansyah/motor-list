package com.mwildaaan.retrofittestingapi.Model.Item;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SportItemIC extends MotorItem{

	@SerializedName("gear")
	@Expose
	protected Integer gear;

	public Integer getGear() {
		return gear;
	}

	public void setGear(Integer gear) {
		this.gear = gear;
	}

	public SportItemIC(){}


	public SportItemIC(String motorType, int price, String mesinCapacity, int id, String urlImage, int gear) {
		this.motorType = motorType;
		this.price = price;
		this.mesinCapacity = mesinCapacity;
		this.id = id;
		this.urlImage = urlImage;
		this.gear = gear;
	}
}