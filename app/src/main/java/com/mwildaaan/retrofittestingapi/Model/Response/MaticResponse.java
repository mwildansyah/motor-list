package com.mwildaaan.retrofittestingapi.Model.Response;

import com.google.gson.annotations.SerializedName;
import com.mwildaaan.retrofittestingapi.Model.Item.MaticItemIC;

import java.util.List;

public class MaticResponse {
    @SerializedName("matic")
    private List<MaticItemIC> matic;

    public void setMatic(List<MaticItemIC> matic){
        this.matic= matic;
    }

    public List<MaticItemIC> getMatic(){
        return matic;
    }

    @Override
    public String toString(){
        return
                "MaticResponse{" +
                        "matic= '" + matic+ '\'' +
                        "}";
    }
}
