package com.mwildaaan.retrofittestingapi.Model.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mwildaaan.retrofittestingapi.Model.Item.BebekItemIC;

import java.util.List;

public class BebekResponse {

    @SerializedName("bebek")
    @Expose
    private List<BebekItemIC> bebek = null;

    public List<BebekItemIC> getBebek() {
        return bebek;
    }

    public void setBebek(List<BebekItemIC> bebek) {
        this.bebek = bebek;
    }

}