package com.mwildaaan.retrofittestingapi.Model.Item;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MotorItem{

    @SerializedName("id")
    @Expose
    protected Integer id;
    @SerializedName("motor_type")
    @Expose
    protected String motorType;
    @SerializedName("url_image")
    @Expose
    protected String urlImage;
    @SerializedName("price")
    @Expose
    protected Integer price;
    @SerializedName("mesin_capacity")
    @Expose
    protected String mesinCapacity;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMotorType() {
        return motorType;
    }

    public void setMotorType(String motorType) {
        this.motorType = motorType;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getMesinCapacity() {
        return mesinCapacity;
    }

    public void setMesinCapacity(String mesinCapacity) {
        this.mesinCapacity = mesinCapacity;
    }
}
