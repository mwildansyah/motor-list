package com.mwildaaan.retrofittestingapi.Model.Response;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.mwildaaan.retrofittestingapi.Model.Item.SportItemIC;

public class SportsResponse{

	@SerializedName("sport")
	private List<SportItemIC> sport;

	public void setSport(List<SportItemIC> sport){
		this.sport = sport;
	}

	public List<SportItemIC> getSport(){
		return sport;
	}

	@Override
 	public String toString(){
		return 
			"SportsResponse{" + 
			"sport = '" + sport + '\'' + 
			"}";
		}
}