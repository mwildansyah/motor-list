package com.mwildaaan.retrofittestingapi.Model.Login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginResponse {
    @SerializedName("loginMeta")
    @Expose
    private LoginMeta loginMeta;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("last_updated")
    @Expose
    private String lastUpdated;
    @SerializedName("loginData")
    @Expose
    private LoginData loginData;

    public LoginMeta getLoginMeta() {
        return loginMeta;
    }

    public void setLoginMeta(LoginMeta loginMeta) {
        this.loginMeta = loginMeta;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public LoginData getLoginData() {
        return loginData;
    }

    public void setLoginData(LoginData loginData) {
        this.loginData = loginData;
    }
}
