package com.mwildaaan.retrofittestingapi.Model.Item;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MaticItemIC extends MotorItem{

    public MaticItemIC(){}

    public MaticItemIC(String motorType, int price, String mesinCapacity, int id, String urlImage) {
        this.motorType = motorType;
        this.price = price;
        this.mesinCapacity = mesinCapacity;
        this.id = id;
        this.urlImage = urlImage;
    }
}
