//package com.mwildaaan.retrofittestingapi.BluetoothPrinter;
//
//import android.app.Activity;
//import android.app.ProgressDialog;
//import android.bluetooth.BluetoothAdapter;
//import android.bluetooth.BluetoothDevice;
//import android.bluetooth.BluetoothSocket;
//import android.content.Intent;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.os.Bundle;
//import android.util.Log;
//import android.view.View;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.TextView;
//import android.widget.Toast;
//import com.mwildaaan.retrofittestingapi.R;
//
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.OutputStream;
//import java.util.Set;
//import java.util.UUID;
//
//
//
//public class BluetoothPrinter extends Activity {
//
//    static private final int REQUEST_ENABLE_BT = 0;
//    private static final UUID SPP_UUID = UUID
//            .fromString("00001101-0000-1000-8000-00805F9B34FB");
//    private static BluetoothSocket btsocket;
//    BluetoothDevice btdevice;
//    BluetoothAdapter mbtAdapter;
//    ProgressDialog loading;
//
//    private static OutputStream outputStream;
//    InputStream inputStream;
//    Thread thread;
//
//    private Button btnTurnOn;
//
//    byte[] readBuffer;
//    int readBufferPosition;
//    volatile boolean stopWorker;
//
//    TextView lblPrinterName, tvStatus, tvPaired, tvPrintStats;
//    EditText textBox;
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.bluetooth_printer);
//        // Create object of controls
//        Button btnTurnOn= (Button) findViewById(R.id.btnTurnOn);
//        Button btnSearch= (Button) findViewById(R.id.btnSearch);
//        Button btnPrint = (Button) findViewById(R.id.btnPrint);
//
//        textBox = (EditText) findViewById(R.id.txtText);
//
//        lblPrinterName = (TextView) findViewById(R.id.lblPrinterName);
//        tvStatus = (TextView) findViewById(R.id.tvStatus);
//        tvPaired = (TextView) findViewById(R.id.tvPaired);
//        tvPrintStats = (TextView) findViewById(R.id.tvPrintStats);
//
//        mbtAdapter = BluetoothAdapter.getDefaultAdapter();
//        if (mbtAdapter.isEnabled()){
//            tvStatus.setText("Bluetooth Status : ON");
//            Button btnStatus = (Button)findViewById(R.id.btnTurnOn);
//            btnStatus.setEnabled(false);
//        }
//
//        btnTurnOn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                try{
//                    TurnOnBluetooth();
//                }catch (Exception ex){
//                    ex.printStackTrace();
//                }
//            }
//        });
//
//        btnSearch.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                try{
//                    SearchBluetooth();
//                }catch (Exception ex){
//                    ex.printStackTrace();
//                }
//            }
//        });
//
//        btnPrint.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                try{
//                    if (btsocket == null){
//                        Set<BluetoothDevice>pairedDevice = mbtAdapter.getBondedDevices();
//                        if(pairedDevice.size()>0){
//                            for(BluetoothDevice pairedDev:pairedDevice){
//                                if(pairedDev.getBondState() == 12){
//                                    btsocket=pairedDev.createRfcommSocketToServiceRecord(SPP_UUID);
//                                    btsocket.connect();
//                                    btdevice=pairedDev;
//                                    lblPrinterName.setText("Bluetooth Printer Attached: "+pairedDev.getName());
//                                    Button btnSearch = (Button)findViewById(R.id.btnSearch);
//                                    btnSearch.setEnabled(false);
//                                    break;
//                                }
//                            }
//                        }
//                    }
//                    if (btsocket != null){
//                        printData();
//                    }
//                }catch (Exception ex){
//                    ex.printStackTrace();
//                }
//            }
//        });
//
//    }
//
//    private void SearchBluetooth() {
//        if(btsocket == null){
//            Intent BTIntent = new Intent(getApplicationContext(), DeviceList.class);
//            this.startActivityForResult(BTIntent, DeviceList.REQUEST_CONNECT_BT);
//        }else {
//            OutputStream opstream = null;
//            try {
//                opstream = btsocket.getOutputStream();
//                outputStream.flush();
//            }catch (IOException ex){
//                ex.printStackTrace();
//            }
//            outputStream = opstream;
//        }
//
//    }
//
//    protected void TurnOnBluetooth(){
//        mbtAdapter = BluetoothAdapter.getDefaultAdapter();
//        if (mbtAdapter == null){
//            Toast.makeText(this, "Bluetooth not supported!!", Toast.LENGTH_SHORT).show();
//        }
//        try{
//            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
//            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
//        }catch (Exception ex){
//            ex.printStackTrace();
//        }
//    }
//
//    // Printing Photo to Bluetooth Printer //
//    void printPhoto(int img, int bg) {
//                try {
//                    Bitmap bmp = BitmapFactory.decodeResource(getResources(), img);
//                    if(bmp!=null){
//                        byte[] command = Utils.decodeBitmap(bmp, bg);
//                        outputStream.write(PrinterCommands.ESC_ALIGN_CENTER);
//                        printText(command);
//                    }else{
//                        Log.e("Print Photo error", "the file isn't exists");
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    Log.e("PrintTools", "the file isn't exists");
//                }
//
//    }
//
//    //print new line
//    private void printNewLine() {
//        try {
//            outputStream.write(PrinterCommands.FEED_LINE);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//    }
//
//    //print text
//    private void printText(String msg) {
//        try {
//            // Print normal text
//            outputStream.write(msg.getBytes());
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//    }
//
//    //print byte[]
//    private void printText(byte[] msg) {
//        try {
//            // Print normal text
//            outputStream.write(msg);
//            printNewLine();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//
//    //print custom
//    private void printCustom(String msg, int size, int align) {
//        //Print config "mode"
//        byte[] cc = new byte[]{0x1B,0x21,0x03};  // 0- normal size text
//        //byte[] cc1 = new byte[]{0x1B,0x21,0x00};  // 0- normal size text
//        byte[] bb = new byte[]{0x1B,0x21,0x08};  // 1- only bold text
//        byte[] bb2 = new byte[]{0x1B,0x21,0x20}; // 2- bold with medium text
//        byte[] bb3 = new byte[]{0x1B,0x21,0x10}; // 3- bold with large text
//        try {
//            switch (size){
//                case 0:
//                    outputStream.write(cc);
//                    break;
//                case 1:
//                    outputStream.write(bb);
//                    break;
//                case 2:
//                    outputStream.write(bb2);
//                    break;
//                case 3:
//                    outputStream.write(bb3);
//                    break;
//            }
//
//            switch (align){
//                case 0:
//                    //left align
//                    outputStream.write(PrinterCommands.ESC_ALIGN_LEFT);
//                    break;
//                case 1:
//                    //center align
//                    outputStream.write(PrinterCommands.ESC_ALIGN_CENTER);
//                    break;
//                case 2:
//                    //right align
//                    outputStream.write(PrinterCommands.ESC_ALIGN_RIGHT);
//                    break;
//            }
//            outputStream.write(msg.getBytes());
//            outputStream.write(PrinterCommands.LF);
//            //outputStream.write(cc);
//            //printNewLine();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//    }
//
//    // Printing Text to Bluetooth Printer //
//    void printData(){
//        try{
//            OutputStream opstream = null;
//            try {
//                opstream = btsocket.getOutputStream();
//            }catch (IOException ex){
//                ex.printStackTrace();
//            }
//            outputStream = opstream;
//            try {
//                outputStream = btsocket.getOutputStream();
//
//                printPhoto(R.drawable.ic_rex_header_print,0  );
//                printCustom("Sport List",1,1);
//                printCustom("Motor Type : ",0,0);
//                printPhoto(R.drawable.ic_rex_histories_transaksi,1);
//                printPhoto(R.drawable.ic_rex_logo,2);
//
//                tvPrintStats.setText("Printing Text...");
//
//                Toast.makeText(BluetoothPrinter.this, "Printing.......", Toast.LENGTH_SHORT).show();
//                outputStream.flush();
//            }catch (IOException ex){
//                ex.printStackTrace();
//            }
//
//        }catch (Exception ex){
//            ex.printStackTrace();
//        }
//    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        switch (requestCode){
//            case REQUEST_ENABLE_BT:
//                if (resultCode == RESULT_OK) {
//                    tvStatus.setText("Bluetooth Status : ON");
//                    Button btnTurnOn = (Button)findViewById(R.id.btnTurnOn);
//                    btnTurnOn.setEnabled(false);
//                }
//        }
//
//        try {
//            btsocket = DeviceList.getSocket();
//            btdevice = DeviceList.getDevices();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        if (btsocket != null){
//            Button btnSearch = (Button)findViewById(R.id.btnSearch);
//            btnSearch.setEnabled(false);
//        }
//
//        if (btdevice != null){
//            tvPaired.setText("Bluetooth Paired : " + btdevice.getName() +"  "+ btdevice.getAddress());
//        }
//    }
//}