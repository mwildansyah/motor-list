package com.mwildaaan.retrofittestingapi.Fragment;

import com.mwildaaan.retrofittestingapi.Activity.MainActivity;
import com.mwildaaan.retrofittestingapi.Adapter.MotorAdapter;
import com.mwildaaan.retrofittestingapi.Model.Response.BebekResponse;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FragmentBebekFC extends FragmentCore {

    @Override
    protected void setCallApi() {
        mApiInterface.getBebekList().enqueue(new Callback<BebekResponse>() {
            @Override
            public void onResponse(Call<BebekResponse> call, Response<BebekResponse> response) {
//                listMotor.addAll(response.body().getBebek());
//                List<MotorItem> listMotorAdapter = new ArrayList<>();
                listMotor.addAll(response.body().getBebek());
                if(getActivity() instanceof MainActivity) {
                    MainActivity mainActivity = (MainActivity) getmActivity();
                    adapter = new MotorAdapter(getActivity(), mainActivity.getBluetoothPrinter(), listMotor);
                }
                mRecycerView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<BebekResponse> call, Throwable t) {

            }
        });
    }
}
