package com.mwildaaan.retrofittestingapi.Fragment;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mwildaaan.retrofittestingapi.Adapter.MotorAdapter;
import com.mwildaaan.retrofittestingapi.Model.Item.MotorItem;
import com.mwildaaan.retrofittestingapi.Network.ApiClient;
import com.mwildaaan.retrofittestingapi.Network.ApiInterface;
import com.mwildaaan.retrofittestingapi.R;

import java.util.ArrayList;
import java.util.List;


public abstract class FragmentCore extends Fragment {
    protected View v;
    protected RecyclerView mRecycerView;
    protected MotorAdapter adapter;
    protected ApiInterface mApiInterface;
    protected Button btnPrint;
    private AppCompatActivity activity;

    protected List<MotorItem> listMotor = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.rv_list, container, false);
        setActivity((AppCompatActivity) getActivity());
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRecycerView = (RecyclerView)v.findViewById(R.id.rv_bebek_id);
        mRecycerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mApiInterface = ApiClient.getService();
        setCallApi();
    }

    protected abstract void setCallApi();


    public AppCompatActivity getmActivity() {
        return activity;
    }

    public void setActivity(AppCompatActivity activity) {
        this.activity = activity;
    }
}
