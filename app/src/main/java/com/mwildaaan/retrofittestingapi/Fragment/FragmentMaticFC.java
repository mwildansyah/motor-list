package com.mwildaaan.retrofittestingapi.Fragment;

import android.widget.Toast;

import com.mwildaaan.retrofittestingapi.Activity.MainActivity;
import com.mwildaaan.retrofittestingapi.Adapter.MotorAdapter;
import com.mwildaaan.retrofittestingapi.Model.Response.MaticResponse;
import com.mwildaaan.retrofittestingapi.Network.ApiClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentMaticFC extends FragmentCore {

    @Override
    protected void setCallApi() {
        ApiClient.getService().getMaticList().enqueue(new Callback<MaticResponse>() {
            @Override
            public void onResponse(Call<MaticResponse> call, Response<MaticResponse> response) {
                if (response.isSuccessful()) {
                    listMotor.addAll(response.body().getMatic());

                    if(getActivity() instanceof MainActivity) {
                        MainActivity mainActivity = (MainActivity) getmActivity();
                        adapter = new MotorAdapter(getActivity(), mainActivity.getBluetoothPrinter(), listMotor);
                    }
                    mRecycerView.setAdapter(adapter);
                } else {
                    Toast.makeText(getActivity(), "Gagal Mengambil Data!!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MaticResponse> call, Throwable t) {
            }
        });
    }
}
