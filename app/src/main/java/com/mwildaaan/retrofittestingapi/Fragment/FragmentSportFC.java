package com.mwildaaan.retrofittestingapi.Fragment;

import com.mwildaaan.retrofittestingapi.Activity.MainActivity;
import com.mwildaaan.retrofittestingapi.Adapter.MotorAdapter;
import com.mwildaaan.retrofittestingapi.Model.Response.SportsResponse;
import com.mwildaaan.retrofittestingapi.Network.ApiClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentSportFC extends FragmentCore {

    @Override
    protected void setCallApi() {

        ApiClient.getService().getSportsList().enqueue(new Callback<SportsResponse>() {
            @Override
            public void onResponse(Call<SportsResponse> call, Response<SportsResponse> response) {
                listMotor.addAll(response.body().getSport());
                if(getActivity() instanceof MainActivity) {
                    MainActivity mainActivity = (MainActivity) getmActivity();
                    adapter = new MotorAdapter(getActivity(), mainActivity.getBluetoothPrinter(), listMotor);
                }
                mRecycerView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<SportsResponse> call, Throwable t) {

            }
        });
    }
}
