package com.mwildaaan.retrofittestingapi.Network;

import com.mwildaaan.retrofittestingapi.Model.Response.BebekResponse;
import com.mwildaaan.retrofittestingapi.Model.Response.MaticResponse;
//import com.mwildaaan.retrofittestingapi.Model.Response.NakedResponse;
import com.mwildaaan.retrofittestingapi.Model.Response.SportsResponse;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiInterface {
    @GET("api/v1/SPORT")
    Call<SportsResponse> getSportsList();

    @GET("api/v1/matic")
    Call<MaticResponse> getMaticList();

    @GET("api/v1/bebek")
    Call<BebekResponse> getBebekList();

    @FormUrlEncoded
    @POST("api/v1/login")
    Call<ResponseBody> loginRequest(@Field("email") String email,
                                    @Field("password") String password);
}