package com.mwildaaan.retrofittestingapi.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mwildaaan.retrofittestingapi.Activity.SettingActivity;
import com.mwildaaan.retrofittestingapi.Model.Item.BebekItemIC;
import com.mwildaaan.retrofittestingapi.Model.Item.MaticItemIC;
import com.mwildaaan.retrofittestingapi.Model.Item.MotorItem;
import com.mwildaaan.retrofittestingapi.Model.Item.SportItemIC;
import com.mwildaaan.retrofittestingapi.R;

import java.util.List;

import id.co.atozpay.atozprinter.BluetoothPrinter;
import id.co.atozpay.atozprinter.DeviceList;

public class MotorAdapter extends RecyclerView.Adapter<MotorAdapter.ViewHolder> {
    private Activity mActivity;
    private BluetoothPrinter bluetoothPrinter;

    private List<MotorItem> listMotor;
    Dialog mDialog;
    private BluetoothSocket btsocket;
    private BluetoothAdapter btAdapter;
    private BluetoothDevice btdevice;
    private Activity activity;
    private TextView tvPaired;
    private int size;

    public MotorAdapter(Activity activity, BluetoothPrinter bluetoothPrinter, List<MotorItem> listMotor) {
        this.listMotor = listMotor;
        this.mActivity = activity;
        this.bluetoothPrinter = bluetoothPrinter;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        MotorItem sampleItem = null;
        if (listMotor.size() > 0) {
            sampleItem = listMotor.get(0);
        }

        if (sampleItem instanceof BebekItemIC) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.bebek_list_fragment, viewGroup, false);
            final ViewHolder vHolder = new ViewHolder(v);
            return vHolder;

        } else if (sampleItem instanceof SportItemIC) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.sport_list_fragment, viewGroup, false);
            final ViewHolder vHolder = new ViewHolder(v);

            return vHolder;

        } else if (sampleItem instanceof MaticItemIC) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.matic_list_fragment, viewGroup, false);
            final ViewHolder vHolder = new ViewHolder(v);
            return vHolder;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        MotorItem sampleItem = listMotor.get(i);
        if (sampleItem instanceof BebekItemIC) {
            BebekItemIC bebekItemIC = (BebekItemIC) sampleItem;
            onBindBebek(viewHolder, bebekItemIC);
        }
        if (sampleItem instanceof SportItemIC) {
            SportItemIC sportItemIC = (SportItemIC) sampleItem;
            onBindSport(viewHolder, sportItemIC);
        }
        if (sampleItem instanceof MaticItemIC) {
            MaticItemIC maticItemIC = (MaticItemIC) sampleItem;
            onBindMatic(viewHolder, maticItemIC);
        }
    }

    private void onBindMatic(final ViewHolder viewHolder, final MaticItemIC maticItemIC) {
        viewHolder.tv_id.setText("ID : " + Integer.toString(maticItemIC.getId()));
        viewHolder.tv_motor_type.setText("Motor Type : " + maticItemIC.getMotorType());
        viewHolder.tv_price.setText("Price : " + Integer.toString(maticItemIC.getPrice()));
        viewHolder.tv_mesin_capacity.setText("Mesin Capacity : " + maticItemIC.getMesinCapacity());
        Glide.with(mActivity).load(maticItemIC.getUrlImage().trim()).into(viewHolder.iv_url_image);
        mDialog = new Dialog(mActivity);
        mDialog.setContentView(R.layout.dialog_matic);

        final MaticItemIC showMatic = (MaticItemIC) listMotor.get(viewHolder.getAdapterPosition());

        viewHolder.matic_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView dialog_motor_type_tv = (TextView) mDialog.findViewById(R.id.tv_motor_type);
                TextView dialog_price_tv = (TextView) mDialog.findViewById(R.id.tv_price);
                TextView dialog_mesin_capacity_tv = (TextView) mDialog.findViewById(R.id.tv_mesin_capacity);
                ImageView dialog_url_image_iv = (ImageView) mDialog.findViewById(R.id.iv_url_image);

                dialog_motor_type_tv.setText(showMatic.getMotorType());
                dialog_price_tv.setText("Price : Rp." + showMatic.getPrice().toString());
                dialog_mesin_capacity_tv.setText("Mesih Capacity : " + showMatic.getMesinCapacity());
                Glide.with(mActivity).load(showMatic.getUrlImage().trim()).into(dialog_url_image_iv);
                mDialog.show();
            }
        });

        viewHolder.btnPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (bluetoothPrinter.getStatusPrinter() == BluetoothPrinter.BLUETOOTH_PAIRED) {
//                    if (DeviceList.getDevices() != null) {
                    size = SettingActivity.getSize();
                    if (size > 3) {
                        size = 3;
                    }
                    bluetoothPrinter.printPhoto(R.drawable.ic_rex_header_print, 0);
                    bluetoothPrinter.printCustom("Matic List", size + 1, 1);
                    bluetoothPrinter.printCustom(showMatic.getMotorType(), size, 0);
                    bluetoothPrinter.printCustom(showMatic.getPrice().toString(), size, 0);
                    bluetoothPrinter.printCustom(showMatic.getMesinCapacity(), size, 0);
                    bluetoothPrinter.printNewLine();
                } else if (bluetoothPrinter.getStatusPrinter() == BluetoothPrinter.BLUETOOTH_ENABLED) {
                    Toast.makeText(mActivity, "Status not paired to device", Toast.LENGTH_SHORT).show();
                } else {
                    bluetoothPrinter.toastStatus(BluetoothPrinter.BLUETOOTH_DISABLED);
                }
            }
        });
    }

    private void onBindSport(final ViewHolder viewHolder, SportItemIC sportItemIC) {
        viewHolder.tv_id.setText("ID : " + Integer.toString(sportItemIC.getId()));
        viewHolder.tv_motor_type.setText("Motor Type : " + sportItemIC.getMotorType());
        viewHolder.tv_price.setText("Price : " + Integer.toString(sportItemIC.getPrice()));
        viewHolder.tv_mesin_capacity.setText("Mesin Capacity : " + sportItemIC.getMesinCapacity());
        viewHolder.tv_gear.setText("Gear : " + Integer.toString(sportItemIC.getGear()));
        Glide.with(mActivity).load(sportItemIC.getUrlImage().trim()).into(viewHolder.iv_url_image);

        mDialog = new Dialog(mActivity);
        mDialog.setContentView(R.layout.dialog_sport);

        final SportItemIC showSport = (SportItemIC) listMotor.get(viewHolder.getAdapterPosition());


        viewHolder.sport_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView dialog_motor_type_tv = (TextView) mDialog.findViewById(R.id.tv_motor_type);
                TextView dialog_price_tv = (TextView) mDialog.findViewById(R.id.tv_price);
                TextView dialog_mesin_capacity_tv = (TextView) mDialog.findViewById(R.id.tv_mesin_capacity);
                TextView dialog_gear_tv = (TextView) mDialog.findViewById(R.id.tv_gear);
                ImageView dialog_url_image_iv = (ImageView) mDialog.findViewById(R.id.iv_url_image);

                dialog_motor_type_tv.setText(showSport.getMotorType());
                dialog_price_tv.setText("Price : Rp." + showSport.getPrice().toString());
                dialog_mesin_capacity_tv.setText("Mesih Capacity : " + showSport.getMesinCapacity());
                dialog_gear_tv.setText("Gear : " + showSport.getGear().toString());
                Glide.with(mActivity).load(showSport.getUrlImage().trim()).into(dialog_url_image_iv);
                mDialog.show();
            }
        });


        viewHolder.btnPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (bluetoothPrinter.getStatusPrinter() == BluetoothPrinter.BLUETOOTH_PAIRED) {
//                    if (DeviceList.getDevices() != null) {
                    size = SettingActivity.getSize();
                    if (size > 3) {
                        size = 3;
                    }
//                    bluetoothPrinter.printPhoto(R.drawable.ic_rex_header_print, 0);
                    bluetoothPrinter.printCustom("Sport List", size + 1, 1);
                    bluetoothPrinter.printCustom(showSport.getMotorType(), size, 0);
                    bluetoothPrinter.printCustom(showSport.getPrice().toString(), size, 0);
                    bluetoothPrinter.printCustom(showSport.getMesinCapacity(), size, 0);
                    bluetoothPrinter.printCustom(showSport.getGear().toString(), size, 0);
                    bluetoothPrinter.printNewLine();
                    bluetoothPrinter.printNewLine();
                } else if (bluetoothPrinter.getStatusPrinter() == BluetoothPrinter.BLUETOOTH_ENABLED) {
                    Toast.makeText(mActivity, "Status not paired to device", Toast.LENGTH_SHORT).show();
                } else {
                    bluetoothPrinter.toastStatus(BluetoothPrinter.BLUETOOTH_DISABLED);
                }
            }
        });
    }

    private void onBindBebek(final ViewHolder viewHolder, final BebekItemIC bebekItemIC) {

        String kopling = Boolean.toString(bebekItemIC.getKopling());
        if (kopling == "true") {
            kopling = "Kopling";
        } else {
            kopling = "Tanpa Kopling";
        }

        viewHolder.tv_id.setText("ID : " + Integer.toString(bebekItemIC.getId()));
        viewHolder.tv_motor_type.setText("Motor Type : " + bebekItemIC.getMotorType());
        viewHolder.tv_price.setText("Price : " + Integer.toString(bebekItemIC.getPrice()));
        viewHolder.tv_mesin_capacity.setText("Mesin Capacity : " + bebekItemIC.getMesinCapacity());
        viewHolder.tv_gear.setText("Gear : " + Integer.toString(bebekItemIC.getGear()));
        viewHolder.tv_kopling.setText("Kopling : " + kopling);
        Glide.with(mActivity).load(bebekItemIC.getUrlImage().trim()).into(viewHolder.iv_url_image);


        mDialog = new Dialog(mActivity);
        mDialog.setContentView(R.layout.dialog_bebek);

        final BebekItemIC showBebek = (BebekItemIC) listMotor.get(viewHolder.getAdapterPosition());

        viewHolder.bebek_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView dialog_motor_type_tv = (TextView) mDialog.findViewById(R.id.tv_motor_type);
                TextView dialog_price_tv = (TextView) mDialog.findViewById(R.id.tv_price);
                TextView dialog_mesin_capacity_tv = (TextView) mDialog.findViewById(R.id.tv_mesin_capacity);
                TextView dialog_gear_tv = (TextView) mDialog.findViewById(R.id.tv_gear);
                TextView dialog_kopling_tv = (TextView) mDialog.findViewById(R.id.tv_kopling);
                ImageView dialog_url_image_iv = (ImageView) mDialog.findViewById(R.id.iv_url_image);

                String kopling = Boolean.toString(showBebek.getKopling());
                if (kopling == "true") {
                    kopling = "Kopling";
                } else {
                    kopling = "Tanpa Kopling";
                }

                dialog_motor_type_tv.setText(showBebek.getMotorType());
                dialog_price_tv.setText("Price : Rp." + showBebek.getPrice().toString());
                dialog_mesin_capacity_tv.setText("Mesih Capacity : " + showBebek.getMesinCapacity());
                dialog_gear_tv.setText("Gear : " + showBebek.getGear().toString());
                dialog_kopling_tv.setText("Kopling : " + kopling);
                Glide.with(mActivity).load(showBebek.getUrlImage().trim()).into(dialog_url_image_iv);
                mDialog.show();
            }
        });

        viewHolder.btnPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (bluetoothPrinter.getStatusPrinter() == BluetoothPrinter.BLUETOOTH_PAIRED) {
//                    if (DeviceList.getDevices() != null) {
                    bluetoothPrinter.printPhoto(R.drawable.ic_rex_header_print, 0);
                    String kopling = Boolean.toString(showBebek.getKopling());
                    if (kopling == "true") {
                        kopling = "Kopling";
                    } else {
                        kopling = "Tanpa Kopling";
                    }
                    size = SettingActivity.getSize();
                    if (size > 3) {
                        size = 3;
                    }
                    bluetoothPrinter.printCustom("Bebek List", size + 1, 1);
                    bluetoothPrinter.printCustom(showBebek.getMotorType(), size, 0);
                    bluetoothPrinter.printCustom(showBebek.getPrice().toString(), size, 0);
                    bluetoothPrinter.printCustom(showBebek.getMesinCapacity(), size, 0);
                    bluetoothPrinter.printCustom(showBebek.getGear().toString(), size, 0);
                    bluetoothPrinter.printCustom(kopling, size, 0);
                    bluetoothPrinter.printNewLine();
                } else if (bluetoothPrinter.getStatusPrinter() == BluetoothPrinter.BLUETOOTH_ENABLED) {
                    Toast.makeText(mActivity, "Status not paired to device", Toast.LENGTH_SHORT).show();
                } else {
                    bluetoothPrinter.toastStatus(BluetoothPrinter.BLUETOOTH_DISABLED);
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return listMotor.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout bebek_list;
        private LinearLayout matic_list;
        private LinearLayout sport_list;
        private ImageView iv_url_image;
        private TextView tv_id;
        private TextView tv_motor_type;
        private TextView tv_price;
        private TextView tv_mesin_capacity;
        private TextView tv_gear;
        private TextView tv_kopling;
        private Button btnPrint;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            btnPrint = (Button) itemView.findViewById(R.id.btnPrint);
            bebek_list = (LinearLayout) itemView.findViewById(R.id.bebek_list_id);
            matic_list = (LinearLayout) itemView.findViewById(R.id.matic_list_id);
            sport_list = (LinearLayout) itemView.findViewById(R.id.sport_list_id);
            iv_url_image = (ImageView) itemView.findViewById(R.id.iv_url_image);
            tv_id = (TextView) itemView.findViewById(R.id.tv_id);
            tv_motor_type = (TextView) itemView.findViewById(R.id.tv_motor_type);
            tv_price = (TextView) itemView.findViewById(R.id.tv_price);
            tv_mesin_capacity = (TextView) itemView.findViewById(R.id.tv_mesin_capacity);
            tv_gear = (TextView) itemView.findViewById(R.id.tv_gear);
            tv_kopling = (TextView) itemView.findViewById(R.id.tv_kopling);
        }
    }


}
